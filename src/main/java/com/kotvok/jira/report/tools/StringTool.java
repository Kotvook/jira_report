package com.kotvok.jira.report.tools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringTool {
    public static String fixJQL(String url){
        return url.replace(" ", "%20")
                .replace(">=", "%3E%3D")
                .replace("\"", "%22")
                .replace("<=", "%3C%3D");
    }

    public static String extractEmail(String input) {
        String regex = "\\(([^@()]+@[^@()]+)\\)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            return matcher.group(1);
        }

        return "Not set";
    }
}