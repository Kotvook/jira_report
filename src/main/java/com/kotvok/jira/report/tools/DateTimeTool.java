package com.kotvok.jira.report.tools;

import com.kotvok.jira.report.entities.Month;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateTimeTool {
    public static String getShortStringDate(Instant date) {
        LocalDateTime startDateTime = LocalDateTime.ofInstant(date, ZoneOffset.UTC);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return formatter.format(startDateTime);
    }

    public static Instant getInstantFromJSONObjectDateTimeFormat(String input) {
        String output = null;

        try {
            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

            output = outputFormatter.format(inputFormatter.parse(input));
        } catch (DateTimeParseException e) {
            e.printStackTrace();
        }

        assert output != null;
        return Instant.parse(output);
    }

    public static Pair<Month, Integer> parseStringToMonthYearPair(String input) {
        Pattern pattern = Pattern.compile("(\\d{3,})\\D+(\\d{1,2})");
        Matcher matcher = pattern.matcher(input);
        LocalDate now = LocalDate.now();
        LocalDate date;

        if (matcher.find()) {
            int year = Integer.parseInt(matcher.group(1));
            int monthNumber = Integer.parseInt(matcher.group(2));

            if (monthNumber > 12) {
                date = LocalDate.of(year, 12, 1);
            } else if (monthNumber < 1) {
                date = LocalDate.of(year, 1, 1);
            } else {
                date = LocalDate.of(year, monthNumber, 1);
            }

            if (date.isAfter(now)) {
                date = now;
            }

        } else {
            date = now;
        }

        return new Pair<>(Month.values()[date.getMonth().getValue() - 1], date.getYear());
    }

    public static Pair<Month, Integer> getPreviousMonth() {
        LocalDate date = LocalDate.now().minusMonths(1);
        return new Pair<>(Month.values()[date.getMonth().getValue() - 1], date.getYear());
    }

    public static Instant getFirstDayOfMonth(Month month, int year) {
        LocalDate date = LocalDate.of(year, month.ordinal() + 1, 1);
        return date.atStartOfDay(ZoneId.systemDefault()).toInstant();
    }

    public static Instant getLastDayOfMonth(Month month, int year) {
        LocalDate date = LocalDate.of(year, month.ordinal() + 1, 1);
        LocalDate startOfNextMonth = date.plusMonths(1);
        return startOfNextMonth.atStartOfDay(ZoneId.systemDefault()).toInstant().minusNanos(1);
    }

    public static String getPreviousMonthRange() {
        LocalDate today = LocalDate.now();
        LocalDate firstDayOfPreviousMonth = today.minusMonths(1).withDayOfMonth(1);
        LocalDate lastDayOfPreviousMonth = today.minusMonths(1).withDayOfMonth(today.minusMonths(1).lengthOfMonth());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        return firstDayOfPreviousMonth.format(formatter) + "-" + lastDayOfPreviousMonth.format(formatter);
    }

    public static String getCurrentFormattedDateTimeString(String pattern) {
        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return currentDateTime.format(formatter);
    }

    public static double getHours(int seconds) {
        double hours = (double) seconds / 3600.0;
        hours = Math.round(hours * 100.0) / 100.0;
        return hours;
    }

    public static String getCustomDate(LocalDate date) {
        return String.format("%s %s %s",
                date.getDayOfMonth(),
                Month.values()[date.getMonth().getValue() - 1],
                date.getYear()
        );
    }
}