package com.kotvok.jira.report.tools;

import builders.dsl.spreadsheet.api.Color;
import builders.dsl.spreadsheet.api.Keywords;
import builders.dsl.spreadsheet.builder.api.CellDefinition;

import static builders.dsl.spreadsheet.api.Color.black;
import static builders.dsl.spreadsheet.api.Keywords.*;
import static builders.dsl.spreadsheet.api.Keywords.right;
import static builders.dsl.spreadsheet.api.Keywords.thin;

public class StyleTools {
    public static void fontSize(int size, CellDefinition cell) {
        cell.style(st -> st.font(font -> font.size(size)));
    }

    public static void fontColor(Color color, CellDefinition cell) {
        cell.style(st -> st.font(font -> font.color(color)));
    }

    public static void bold(CellDefinition cell) {
        cell.style(style -> style.font(font -> font.style(bold)));
    }

    public static void italic(CellDefinition cell) {
        cell.style(style -> style.font(font -> font.style(italic)));
    }

    public static void backgroundColor(String color, CellDefinition cell) {
        cell.style(style -> style.background(color));
    }

    public static void setAroundThinBlackBorders(CellDefinition c) {
        c.style(st -> {
            st.border(top, bottom, b -> {
                b.style(thin);
                b.color(black);
            });
            st.border(left, right, b -> {
                b.style(thin);
                b.color(black);
            });
        });
    }

    public static void setThinBlackBorder(Keywords.BorderSide side, CellDefinition c) {
        c.style(st -> st.border(side, b -> {
            b.style(thin);
            b.color(black);
        }));
    }

    public static void alignCenter(CellDefinition cell) {
        cell.style(st -> st.align(VerticalAlignment.CENTER, HorizontalAlignment.CENTER));
    }

    public static void alignLeft(CellDefinition cell) {
        cell.style(st -> st.align(VerticalAlignment.BOTTOM, HorizontalAlignment.LEFT));
    }
}