package com.kotvok.jira.report.controllers;

import com.kotvok.jira.report.services.ReportJiraService;
import com.kotvok.jira.report.tools.DateTimeTool;
import com.kotvok.jira.report.services.ReportExelService;
import com.kotvok.jira.report.entities.Month;
import com.kotvok.jira.report.tools.Pair;
import com.kotvok.jira.report.entities.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/jira/get-report/{projectName}")
public class JiraReportController {
    private final ReportJiraService reportJiraService;
    private final ReportExelService reportExelService;

    @Autowired
    public JiraReportController(ReportJiraService reportJiraService, ReportExelService reportExelService) {
        this.reportJiraService = reportJiraService;
        this.reportExelService = reportExelService;
    }

    @GetMapping({"/{yearMonth}","/{yearMonth}/", "/{yearMonth}/{addParameter}"})
    public ResponseEntity<byte[]> generateAndSendReport(@PathVariable("projectName") String projectName,
                                                        @PathVariable("yearMonth")String yearMonth,
                                                        @PathVariable("addParameter") Optional<String> addParameter) {
        Pair<Month, Integer> monthYear = DateTimeTool.parseStringToMonthYearPair(yearMonth);
        boolean getAll = addParameter.equals(Optional.of("all"));

        List <Role> roleList = reportJiraService
                .getReportList(projectName, monthYear.getFirst(), monthYear.getSecond(), getAll);

        byte[] reportBytes = reportExelService
                .createReportSheetAsByteArray(
                        roleList,
                        monthYear.getFirst() + " " + monthYear.getSecond());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));

        String fileName = String.format(
                "Month report (%s %s) - %s.xlsx",
                monthYear.getFirst(),
                monthYear.getSecond(),
                DateTimeTool.getCurrentFormattedDateTimeString("dd.MM.yyyy_HH-mm-ss")
        );

        headers.setContentDispositionFormData("attachment", fileName);

        return new ResponseEntity<>(reportBytes, headers, HttpStatus.OK);
    }
}
