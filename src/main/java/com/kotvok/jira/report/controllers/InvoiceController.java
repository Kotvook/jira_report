package com.kotvok.jira.report.controllers;

import com.kotvok.jira.report.config.CompanyConfig;
import com.kotvok.jira.report.config.InvoicePersonsConfig;
import com.kotvok.jira.report.entities.Company;
import com.kotvok.jira.report.entities.Month;
import com.kotvok.jira.report.entities.Person;
import com.kotvok.jira.report.entities.Role;
import com.kotvok.jira.report.services.InvoiceExelService;
import com.kotvok.jira.report.services.ReportJiraService;
import com.kotvok.jira.report.tools.DateTimeTool;
import com.kotvok.jira.report.tools.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {

    private final InvoiceExelService INVOICE_EXEL_SERVICE;
    private final ReportJiraService JIRA_REPORT_SERVICE;
    private final CompanyConfig COMPANY_SET;
    private final InvoicePersonsConfig INVOICE_PERSONS_CONFIG;

    @Autowired
    public InvoiceController(InvoiceExelService invoiceExelService,
                             ReportJiraService reportJiraService,
                             CompanyConfig companyConfig,
                             InvoicePersonsConfig invoicePersonsConfig) {
        INVOICE_EXEL_SERVICE = invoiceExelService;
        JIRA_REPORT_SERVICE = reportJiraService;
        COMPANY_SET = companyConfig;
        INVOICE_PERSONS_CONFIG = invoicePersonsConfig;
    }

    @GetMapping("/get-report/{projectKey}")
    public ResponseEntity<byte[]> getReport(@PathVariable("projectKey") String projectKey) {
        Company supplier = COMPANY_SET.getSupplier();
        Company customer = COMPANY_SET.getCustomer();
        Pair<Company, Company> companyPair = new Pair<>(supplier, customer);

        Pair<Month, Integer> previousMonth = DateTimeTool.getPreviousMonth();
        List<Role> roleList = JIRA_REPORT_SERVICE
                .getReportList(projectKey,
                        previousMonth.getFirst(),
                        previousMonth.getSecond(),
                        false);

        roleList.forEach(role -> {
            List<Person> invoicePersons = INVOICE_PERSONS_CONFIG.getPersonsByConfig(role.getPersonList());
            role.setPersonList(invoicePersons);
        });

        String title = projectKey + " " + previousMonth.getFirst().name();
        byte[] invoiceBytes = INVOICE_EXEL_SERVICE.create(companyPair, roleList, title);

        String fileName = String.format(
                "InvoiceB %s - %s %s (%s).xlsx",
                projectKey,
                previousMonth.getSecond(),
                previousMonth.getFirst().name(),
                DateTimeTool.getCurrentFormattedDateTimeString("dd.MM.yyyy_HH-mm-ss")
        );

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
        headers.setContentDispositionFormData("attachment", fileName);

        return new ResponseEntity<>(invoiceBytes, headers, HttpStatus.OK);
    }
}