package com.kotvok.jira.report.config;

import com.kotvok.jira.report.entities.Person;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "invoice")
public class InvoicePersonsConfig {
    private List<Person> persons;

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public List<Person> getPersonsByConfig(List<Person> inputPersons) {
        List<Person> filteredPersons = new ArrayList<>();
        for (Person inputPerson : inputPersons) {
            for (Person thisPerson : this.persons) {
                if (inputPerson.getEmail().equals(thisPerson.getEmail())) {
                    inputPerson.setPrice(thisPerson.getPrice());
                    inputPerson.setVatRate(thisPerson.getVatRate());
                    filteredPersons.add(inputPerson);
                    break;
                }
            }
        }
        return filteredPersons;
    }
}