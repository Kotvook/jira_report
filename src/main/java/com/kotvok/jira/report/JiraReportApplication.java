package com.kotvok.jira.report;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.math.RoundingMode;

@SpringBootApplication
public class JiraReportApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiraReportApplication.class, args);
    }
}