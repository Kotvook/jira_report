package com.kotvok.jira.report.services;

import builders.dsl.spreadsheet.api.Keywords;
import builders.dsl.spreadsheet.builder.api.SheetDefinition;
import builders.dsl.spreadsheet.builder.poi.PoiSpreadsheetBuilder;
import com.kotvok.jira.report.entities.Company;
import com.kotvok.jira.report.entities.Person;
import com.kotvok.jira.report.entities.Role;
import com.kotvok.jira.report.tools.DateTimeTool;
import com.kotvok.jira.report.tools.NumberToWords;
import com.kotvok.jira.report.tools.Pair;
import com.kotvok.jira.report.tools.StyleTools;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

import static builders.dsl.spreadsheet.api.Color.gray;
import static builders.dsl.spreadsheet.api.Keywords.bottom;
import static builders.dsl.spreadsheet.api.Keywords.left;
import static builders.dsl.spreadsheet.api.Keywords.right;
import static builders.dsl.spreadsheet.api.Keywords.text;
import static builders.dsl.spreadsheet.api.Keywords.top;

@Service
public class InvoiceExelService {
    private BigDecimal totalAmount;
    private BigDecimal totalInclVat;
    private final String softBlue = "#ccecff";
    private final String greenBlue = "#daeef3";
    private final int columnAWidth = 25;
    private final int columnBWidth = 30;
    private final int columnCWidth = 10;
    private final int columnDWidth = 10;
    private final int columnEWidth = 20;
    private final int columnFWidth = 30;

    public byte[] create(Pair<Company, Company> company, List<Role> roleList, String title) {

        totalAmount = BigDecimal.valueOf(0);
        totalInclVat = BigDecimal.valueOf(0);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PoiSpreadsheetBuilder.create(byteArrayOutputStream).build(workbook ->
                workbook.sheet(title, sheet -> {

                    // 0
                    setColumnsWidth(sheet);

                    // 1
                    writeDates(sheet);

                    // 2
                    writeCompaniesData(company, sheet);

                    // 3
                    writePeriod(sheet);

                    // 4
                    writeDescriptionHeader(sheet);

                    // 5
                    writePersonData(roleList, sheet);

                    // 6
                    writeTotal(sheet);

                    // 7
                    writeVAT(sheet);

                    // 8
                    writeTotalInclVAT(sheet);

                    //9
                    writeAmountInWords(sheet);

                    //10
                    writeVatAmount(sheet);

                    //11
                    writeSignatures(company, sheet);
                }));

        return byteArrayOutputStream.toByteArray();
    }

    // 0
    private void setColumnsWidth(SheetDefinition sheet) {
        sheet.row(row -> {
            row.cell(cell -> cell.width(columnAWidth));
            row.cell(cell -> cell.width(columnBWidth));
            row.cell(cell -> cell.width(columnCWidth));
            row.cell(cell -> cell.width(columnDWidth));
            row.cell(cell -> cell.width(columnEWidth));
            row.cell(cell -> cell.width(columnFWidth));
        });
    }

    // 1
    private void writeDates(SheetDefinition sheet) {
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("Invoice");
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.bold(cell);
                StyleTools.setAroundThinBlackBorders(cell);
            });
            row.cell(cell -> {
                cell.text(DateTimeTool.getCurrentFormattedDateTimeString("MM-yyyy"));
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.bold(cell);
                StyleTools.fontSize(14, cell);
                StyleTools.setAroundThinBlackBorders(cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("Issue date");
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.bold(cell);
                StyleTools.fontSize(12, cell);
                StyleTools.setAroundThinBlackBorders(cell);
            });
            row.cell(cell -> {
                cell.text(DateTimeTool.getCustomDate(LocalDate.now()));
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.bold(cell);
                StyleTools.fontSize(12, cell);
                StyleTools.setAroundThinBlackBorders(cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("Due date");
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.bold(cell);
                StyleTools.fontSize(12, cell);
                StyleTools.setAroundThinBlackBorders(cell);
            });

            row.cell(cell -> {
                cell.text(DateTimeTool.getCustomDate(LocalDate.now().plusDays(25)));
                StyleTools.backgroundColor(greenBlue, cell);
                StyleTools.bold(cell);
                StyleTools.fontSize(12, cell);
                StyleTools.setAroundThinBlackBorders(cell);
            });
        });
    }

    // 2
    private void writeCompaniesData(Pair<Company, Company> company, SheetDefinition sheet) {
        sheet.row(6, row -> {
            row.cell(cell -> {
                cell.text("");
                StyleTools.setThinBlackBorder(top, cell);
            });
            row.cell(cell -> {
                cell.text("Supplier:");
                cell.colspan(3);
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.fontSize(14, cell);
                StyleTools.bold(cell);
                StyleTools.setAroundThinBlackBorders(cell);
            });
            row.cell(cell -> {
                cell.text("Customer:");
                cell.colspan(2);
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.fontSize(14, cell);
                StyleTools.bold(cell);
                StyleTools.setAroundThinBlackBorders(cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("company name");
                StyleTools.fontSize(8, cell);
                StyleTools.fontColor(gray, cell);
                StyleTools.italic(cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getFirst().getName());
                cell.colspan(3);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getSecond().getName());
                cell.colspan(2);
                StyleTools.setThinBlackBorder(right, cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("company code");
                StyleTools.fontSize(8, cell);
                StyleTools.fontColor(gray, cell);
                StyleTools.italic(cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getFirst().getCode());
                cell.colspan(3);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getSecond().getCode());
                cell.colspan(2);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("company address");
                StyleTools.fontSize(8, cell);
                StyleTools.fontColor(gray, cell);
                StyleTools.italic(cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getFirst().getAddress());
                cell.colspan(3);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getSecond().getAddress());
                cell.colspan(2);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("VAT ID");
                StyleTools.fontSize(8, cell);
                StyleTools.fontColor(gray, cell);
                StyleTools.italic(cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getFirst().getVatId());
                cell.colspan(3);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getSecond().getVatId());
                cell.colspan(2);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("Bank");
                StyleTools.fontSize(8, cell);
                StyleTools.fontColor(gray, cell);
                StyleTools.italic(cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getFirst().getBankName());
                cell.colspan(3);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getSecond().getBankName());
                cell.colspan(2);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.colspan(3);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.colspan(2);
                StyleTools.setThinBlackBorder(right, cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("Bank account");
                StyleTools.fontSize(8, cell);
                StyleTools.fontColor(gray, cell);
                StyleTools.italic(cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getFirst().getBankAccount());
                cell.colspan(3);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getSecond().getBankAccount());
                cell.colspan(2);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("Bank address");
                StyleTools.fontSize(8, cell);
                StyleTools.fontColor(gray, cell);
                StyleTools.italic(cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getFirst().getBankAddress());
                cell.colspan(3);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getSecond().getBankAddress());
                cell.colspan(2);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("SWIFT");
                StyleTools.fontSize(8, cell);
                StyleTools.fontColor(gray, cell);
                StyleTools.italic(cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getFirst().getSwift());
                cell.colspan(3);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getSecond().getSwift());
                cell.colspan(2);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("Corr Bank + address");
                StyleTools.fontSize(8, cell);
                StyleTools.fontColor(gray, cell);
                StyleTools.italic(cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getFirst().getCorrBankAndAddress());
                cell.colspan(3);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
            row.cell(cell -> {
                cell.value(company.getSecond().getCorrBankAndAddress());
                cell.colspan(2);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("Corr Bank account + SWIFT");
                StyleTools.fontSize(8, cell);
                StyleTools.fontColor(gray, cell);
                StyleTools.italic(cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(right, cell);
                StyleTools.setThinBlackBorder(bottom, cell);
            });
            row.cell(cell -> {
                cell.value(company.getFirst().getCorrBankAndSwift());
                cell.colspan(3);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
                StyleTools.setThinBlackBorder(bottom, cell);
            });
            row.cell(cell -> {
                cell.value(company.getSecond().getCorrBankAndSwift());
                cell.colspan(2);
                StyleTools.fontSize(12, cell);
                StyleTools.setThinBlackBorder(right, cell);
                StyleTools.setThinBlackBorder(bottom, cell);
            });
        });
    }

    // 3
    private void writePeriod(SheetDefinition sheet) {
        sheet.row(row -> row.cell(cell -> {
            cell.value("Period " + DateTimeTool.getPreviousMonthRange());
            StyleTools.fontSize(8, cell);
        }));
    }

    // 4
    private void writeDescriptionHeader(SheetDefinition sheet) {
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("Description");
                cell.colspan(2);
                cell.height(40);
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.setAroundThinBlackBorders(cell);
                StyleTools.alignCenter(cell);
            });
            row.cell(cell -> {
                cell.text("Quantity");
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.setAroundThinBlackBorders(cell);
                StyleTools.alignCenter(cell);
            });
            row.cell(cell -> {
                cell.text("Unit Price, USD");
                cell.style(st -> st.wrap(text));
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.setAroundThinBlackBorders(cell);
                StyleTools.alignCenter(cell);
            });
            row.cell(cell -> {
                cell.text("VAT rate, %");
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.setAroundThinBlackBorders(cell);
                StyleTools.alignCenter(cell);
            });
            row.cell(cell -> {
                cell.text("Amount, USD");
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.setAroundThinBlackBorders(cell);
                StyleTools.alignCenter(cell);
            });
        });
        sheet.row(row -> row.cell(cell -> {
            cell.colspan(6);
            StyleTools.setThinBlackBorder(right, cell);
        }));
    }

    // 5
    private void writePersonData(List<Role> roleList, SheetDefinition sheet) {
        for (Role role : roleList) {
            for (Person person : role.getPersonList()) {

                double quantity = person.getTotalHours();

                BigDecimal personUnitPrice = person.getPrice();

                BigDecimal personAmount = person.getAmount();
                totalAmount = totalAmount.add(personAmount);
                totalInclVat = totalInclVat.add(person.getAmountWithVat());

                BigDecimal personVatRate = person.getVatRate();

                sheet.row(row -> {
                    row.cell(cell -> {
                        cell.value(role.getRoleName());
                        cell.colspan(2);
                        StyleTools.setAroundThinBlackBorders(cell);
                    });
                    row.cell(cell -> {
                        cell.value(quantity);
                        cell.style(st -> st.align(Keywords.VerticalAlignment.CENTER, Keywords.HorizontalAlignment.LEFT));
                        StyleTools.setAroundThinBlackBorders(cell);
                    });
                    row.cell(cell -> {
                        cell.value(personUnitPrice);
                        cell.style(st -> st.align(Keywords.VerticalAlignment.CENTER, Keywords.HorizontalAlignment.LEFT));
                        StyleTools.setAroundThinBlackBorders(cell);
                    });
                    row.cell(cell -> {
                        cell.value(personVatRate);
                        cell.style(st -> st.align(Keywords.VerticalAlignment.CENTER, Keywords.HorizontalAlignment.LEFT));
                        StyleTools.setAroundThinBlackBorders(cell);
                    });
                    row.cell(cell -> {
                        cell.value(personAmount);
                        StyleTools.setAroundThinBlackBorders(cell);
                    });
                });
            }
        }
    }

    // 6
    private void writeTotal(SheetDefinition sheet) {
        drawTotal(sheet, totalAmount, "Total");
    }

    // 7
    private void writeVAT(SheetDefinition sheet) {
        BigDecimal averageVatRate = totalInclVat
                .divide(totalAmount, 10,RoundingMode.HALF_UP)
                .subtract(BigDecimal.valueOf(1))
                .multiply(BigDecimal.valueOf(100));
        averageVatRate = averageVatRate.setScale(5, RoundingMode.HALF_UP);
        drawTotal(sheet, averageVatRate, "VAT");
    }

    // 8
    private void writeTotalInclVAT(SheetDefinition sheet) {
        drawTotal(sheet, totalInclVat, "Total incl.VAT");
    }

    private void drawTotal(SheetDefinition sheet, BigDecimal value, String title) {
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text(title);
                cell.colspan(2);
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.setThinBlackBorder(top, cell);
                StyleTools.setThinBlackBorder(left, cell);
                StyleTools.setThinBlackBorder(bottom, cell);
            });
            row.cell(cell -> {
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.setThinBlackBorder(top, cell);
                StyleTools.setThinBlackBorder(bottom, cell);
            });
            row.cell(cell -> {
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.setThinBlackBorder(top, cell);
                StyleTools.setThinBlackBorder(bottom, cell);
            });
            row.cell(cell -> {
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.setThinBlackBorder(top, cell);
                StyleTools.setThinBlackBorder(bottom, cell);
            });
            row.cell(cell -> {
                cell.value(value);
                StyleTools.backgroundColor(softBlue, cell);
                StyleTools.setThinBlackBorder(top, cell);
                StyleTools.setThinBlackBorder(right, cell);
                StyleTools.setThinBlackBorder(bottom, cell);
            });
        });
    }

    // 9
    private void writeAmountInWords(SheetDefinition sheet) {
        String currency = "USD";
        String coin = "ct";
        String amount = NumberToWords.Convert(totalInclVat.longValue());
        int coins = totalInclVat
                .remainder(BigDecimal.ONE)
                .setScale(2, RoundingMode.HALF_UP)
                .multiply(new BigDecimal("100")).intValue();

        sheet.row(row -> row.cell(cell -> {
            cell.text("Amount to be paid in words:");
            cell.colspan(2);
        }));
        sheet.row(row -> row.cell(cell -> cell.value(String.format("%s %s %s %s", amount, currency, coins, coin))));
    }

    //10
    private void writeVatAmount(SheetDefinition sheet) {
        BigDecimal vatAmount = totalInclVat.subtract(totalAmount);
        sheet.row(row -> {
            row.cell(cell -> cell.text("VAT amount"));
            row.cell();
            row.cell();
            row.cell();
            row.cell();
            row.cell(cell -> cell.value(vatAmount));
        });
    }

    // 11
    private void writeSignatures(Pair<Company, Company> company, SheetDefinition sheet) {
        sheet.row();
        sheet.row();
        sheet.row();
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("On behalf of a Supplier");
                StyleTools.bold(cell);
            });
            row.cell();
            row.cell();
            row.cell();
            row.cell(cell -> {
                cell.text("On behalf of a Customer");
                StyleTools.bold(cell);
            });
        });
        sheet.row(row -> {
            row.cell();
            row.cell(cell -> {
                cell.value(company.getFirst().getName());
                cell.height(35);
                cell.style(st -> st.align(Keywords.VerticalAlignment.BOTTOM, Keywords.HorizontalAlignment.LEFT));
                StyleTools.bold(cell);
            });
            row.cell();
            row.cell();
            row.cell();
            row.cell(cell -> {
                cell.value(company.getSecond().getName());
                cell.style(st -> st.align(Keywords.VerticalAlignment.BOTTOM, Keywords.HorizontalAlignment.LEFT));
                StyleTools.bold(cell);
            });
        });
        sheet.row(row -> {
            row.cell(cell -> {
                cell.text("signature");
                StyleTools.setThinBlackBorder(top, cell);
            });
            row.cell();
            row.cell();
            row.cell();
            row.cell(cell -> {
                cell.text("signature");
                StyleTools.setThinBlackBorder(top, cell);
            });
        });
    }
}