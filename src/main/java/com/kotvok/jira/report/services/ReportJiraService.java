package com.kotvok.jira.report.services;

import com.kotvok.jira.report.entities.Role;
import com.kotvok.jira.report.entities.Person;
import com.kotvok.jira.report.entities.Task;
import com.kotvok.jira.report.tools.*;
import com.kotvok.jira.report.entities.Month;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.time.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ReportJiraService {

    private final String JIRA_URL;
    private final String USERNAME;
    private final String PASSWORD;
    private final List<String> DEVELOPER_LIST;
    private final List<String> QA_LIST;
    private final String DEVELOPMENT_ROLE;
    private final String QA_ROLE;
    private final String OTHER_ROLE = "Other";

    @Autowired
    public ReportJiraService(@Value("${jira.url}") String jiraUrl,
                             @Value("${jira.username}") String username,
                             @Value("${jira.password}") String password,
                             @Value("${roles.developer.list}") List<String> developerList,
                             @Value("${roles.qa.list}") List<String> qaList,
                             @Value("${roles.developer.naming}") String developmentRole,
                             @Value("${roles.qa.naming}") String qaRole) {
        JIRA_URL = jiraUrl;
        USERNAME = username;
        PASSWORD = password;
        DEVELOPER_LIST = developerList;
        QA_LIST = qaList;
        DEVELOPMENT_ROLE = developmentRole;
        QA_ROLE = qaRole;
    }

    public List<Role> getReportList(String projectKey, Month month, int year, boolean getAll) {

        Map<String, List<Person>> mapRoleToPersonList = new HashMap<>();
        Instant startDate = DateTimeTool.getFirstDayOfMonth(month, year);
        Instant endDate = DateTimeTool.getLastDayOfMonth(month, year);
        String shortStartDate = DateTimeTool.getShortStringDate(startDate);
        String shortEndDate = DateTimeTool.getShortStringDate(endDate);
        String apiRequestGetMonthlyTasks = StringTool.fixJQL(String.format("/rest/api/2/search?jql=project=%s " +
                        "AND worklogDate >= \"%s\" " +
                        "AND worklogDate <= \"%s\"",
                projectKey, shortStartDate, shortEndDate));

        String summaryUrl = JIRA_URL + apiRequestGetMonthlyTasks;
        String auth = USERNAME + ":" + PASSWORD;

        try {
            // Создание HTTP-клиента
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();

            // Добавление заголовка авторизации и выполнение GET-запроса для получения сводных данных
            HttpGet httpGet = new HttpGet(summaryUrl);
            byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes());
            String authHeader = "Basic " + new String(encodedAuth);
            httpGet.setHeader("Authorization", authHeader);

            HttpResponse response = httpClient.execute(httpGet);

            // Выполнение запроса
            HttpEntity httpEntity = response.getEntity();
            String responseString = EntityUtils.toString(httpEntity);

            // Обработка полученного JSON-ответа
            JSONObject jsonResponse = new JSONObject(responseString);
            JSONArray issues = jsonResponse.getJSONArray("issues");

            List<Task> taskList = new ArrayList<>();
            for (int i = issues.length() - 1; i >= 0; i--) {
                JSONObject issue = issues.getJSONObject(i);
                JSONObject fields = issue.getJSONObject("fields");

                // Получение данных о задаче
                String issueKey = issue.getString("key");
                String issueSummary = fields.getString("summary");
                String issueStatus = fields.getJSONObject("status").getString("name");
                String issueParent = fields.optJSONObject("parent", new JSONObject()).optString("key", "");
                String epicLink = fields.optString("customfield_10001", "");

                taskList.add(new Task(issueKey, issueParent, issueSummary, issueStatus, epicLink));
            }

            Map<String, ArrayList<Task>> mapEmailToTaskList = new ConcurrentHashMap<>();
            Map<String, String> mapEmailToPerson = new ConcurrentHashMap<>();

            // перебор ворклогов каждой задачи
            ExecutorService executor = Executors.newFixedThreadPool(10);
            List<CompletableFuture<Void>> futures = new ArrayList<>();
            for (Task task : taskList) {
                CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                    try {
                        String threadApiRequest = JIRA_URL + String.format("/rest/api/2/issue/%s/worklog", task.getNumber());
                        HttpGet threadHttpGet = new HttpGet(URI.create(threadApiRequest));
                        HttpResponse threadHttpResponse = httpClient.execute(threadHttpGet);
                        HttpEntity threadHttpEntity = threadHttpResponse.getEntity();
                        String threadRresponseString = EntityUtils.toString(threadHttpEntity);

                        JSONObject localJsonResponse = new JSONObject(threadRresponseString);
                        JSONArray worklogs = localJsonResponse.getJSONArray("worklogs");

                        // Агрегируем все логи одной задачи
                        // и определяем кто сколько времени потратил на задачу
                        Map<String, Integer> mapEmailToSpentTimeAndDate = getTotalTimeForEveryPersonInTask(startDate, endDate, worklogs, mapEmailToPerson);

                        // распределяем задачу по разным авторам,
                        // в итоге получим карту: email -> список задач
                        distributeTaskToDifferentPersons(mapEmailToTaskList, task, mapEmailToSpentTimeAndDate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }, executor);
                futures.add(future);
            }

            // Ожидание завершения всех CompletableFuture и завершение работы ExecutorService
            CompletableFuture<Void> allOf = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
            allOf.join();
            executor.shutdown();

            // Присвоение роли по email человека
            for (Map.Entry<String, ArrayList<Task>> entryEmailToTaskList : mapEmailToTaskList.entrySet()) {

                if (getRoleByEmail(entryEmailToTaskList.getKey(), getAll) == null) {
                    continue;
                }

                String role = getRoleByEmail(entryEmailToTaskList.getKey(), getAll);
                List<Person> personList = mapRoleToPersonList.get(role);

                if (personList == null) {
                    personList = new ArrayList<>();
                }

                Person person = new Person(
                        mapEmailToPerson.get(entryEmailToTaskList.getKey()),
                        entryEmailToTaskList.getKey(),
                        entryEmailToTaskList.getValue()
                );

                // Сортировка списка задач по затраченному времени
                person.sortTasksBySpentTime();

                personList.add(person);
                mapRoleToPersonList.put(role, personList);
            }

            // Закрытие HTTP-клиента
            httpClient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Сортировка людей по порядку в application.yml,
        // сначала roles.developer.list, потом roles.qa.list, потом все остальные
        return getSortedRoleList(mapRoleToPersonList);
    }

    private static Map<String, Integer> getTotalTimeForEveryPersonInTask(Instant startDate, Instant endDate, JSONArray workLogs, Map<String, String> mapEmailToPerson) {
        Map<String, Integer> mapEmailToSpentTime = new HashMap<>();
        for (int i = 0; i < workLogs.length(); i++) {
            JSONObject workLog = workLogs.getJSONObject(i);

            // Исключаем логи, которые не входят в заданный месяц
            Instant workLogStartDataTime = DateTimeTool.getInstantFromJSONObjectDateTimeFormat(workLog.getString("started"));
            if (!(workLogStartDataTime.isAfter(startDate) && workLogStartDataTime.isBefore(endDate))) {
                continue;
            }

            // Получаем все нужные свойства лога и складываем timeSpentSeconds для каждого author
            // и в итоге получаем карту
            // email -> timeSpentSeconds(суммарное время)
            JSONObject author = workLog.getJSONObject("author");
            String personName = author.getString("displayName");
            String personEmail = author.getString("emailAddress");
            mapEmailToPerson.put(personEmail, personName);
            int timeSpentSeconds = workLog.getInt("timeSpentSeconds");
            if (mapEmailToSpentTime.containsKey(personEmail)) {
                int currentSpentTime = mapEmailToSpentTime.get(personEmail);
                timeSpentSeconds = currentSpentTime + timeSpentSeconds;
            }
            mapEmailToSpentTime.put(personEmail, timeSpentSeconds);
        }
        return mapEmailToSpentTime;
    }

    private static void distributeTaskToDifferentPersons(Map<String, ArrayList<Task>> mapEmailToTaskList, Task task, Map<String, Integer> mapEmailToSpentTime) {
        for (Map.Entry<String, Integer> spentTime : mapEmailToSpentTime.entrySet()) {
            Task taskOfPerson = new Task(task.getNumber(),
                    task.getParent(),
                    task.getName(),
                    task.getStatus(),
                    task.getEpicLink(),
                    spentTime.getValue());

            ArrayList<Task> tasks = mapEmailToTaskList.computeIfAbsent(spentTime.getKey(), t -> new ArrayList<>());
            tasks.add(taskOfPerson);
            mapEmailToTaskList.put(spentTime.getKey(), tasks);
        }
    }

    private List<Role> getSortedRoleList(Map<String, List<Person>> rolePersonMap) {
        List<Role> roleList = new ArrayList<>();
        if (rolePersonMap.get(DEVELOPMENT_ROLE) != null) {
            Role developer = new Role(DEVELOPMENT_ROLE, rolePersonMap.get(DEVELOPMENT_ROLE));
            developer.sortPersonsByEmailList(DEVELOPER_LIST);
            roleList.add(new Role(DEVELOPMENT_ROLE, rolePersonMap.get(DEVELOPMENT_ROLE)));
        }
        if (rolePersonMap.get(QA_ROLE) != null) {
            Role qa = new Role(QA_ROLE, rolePersonMap.get(QA_ROLE));
            qa.sortPersonsByEmailList(QA_LIST);
            roleList.add(new Role(QA_ROLE, rolePersonMap.get(QA_ROLE)));
        }
        if (rolePersonMap.get(OTHER_ROLE) != null) {
            roleList.add(new Role(OTHER_ROLE, rolePersonMap.get(OTHER_ROLE)));
        }
        return roleList;
    }

    private String getRoleByEmail(String email, boolean getAll) {
        if (DEVELOPER_LIST.contains(email)) {
            return DEVELOPMENT_ROLE;
        } else if (QA_LIST.contains(email)) {
            return QA_ROLE;
        } else if (getAll) {
            return OTHER_ROLE;
        } else return null;
    }
}