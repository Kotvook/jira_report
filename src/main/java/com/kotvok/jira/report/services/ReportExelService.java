package com.kotvok.jira.report.services;

import builders.dsl.spreadsheet.api.Keywords;
import builders.dsl.spreadsheet.builder.api.SheetDefinition;
import builders.dsl.spreadsheet.builder.poi.PoiSpreadsheetBuilder;
import com.kotvok.jira.report.entities.Person;
import com.kotvok.jira.report.entities.Task;
import com.kotvok.jira.report.tools.DateTimeTool;
import com.kotvok.jira.report.entities.Role;
import com.kotvok.jira.report.tools.StyleTools;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.*;

import static builders.dsl.spreadsheet.api.Keywords.*;

@Service
public class ReportExelService {
    public byte[] createReportSheetAsByteArray(List<Role> roleList, String title) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PoiSpreadsheetBuilder.create(byteArrayOutputStream).build(workbook ->
                workbook.sheet(title, sheet -> {
                    writeHeader(sheet, title);
                    double grandTotalTime = 0.0;
                    for (Role role : roleList) {
                        writeRole(role.getRoleName(), sheet);
                        grandTotalTime += writeData(role.getRoleName(), role.getPersonList(), sheet);
                    }
                    writeFooter(sheet, grandTotalTime);
                }));
        return byteArrayOutputStream.toByteArray();
    }

    private static void writeHeader(SheetDefinition sheet, String title) {
        sheet.row(r -> {
            r.cell(c -> {
                c.text(title);
                StyleTools.bold(c);
                c.colspan(3);
                StyleTools.setAroundThinBlackBorders(c);
            });
            r.cell(c -> {
                c.text("Total Hours");
                StyleTools.bold(c);
                c.rowspan(2);
                c.width(12);
                StyleTools.setAroundThinBlackBorders(c);
            });

        });
        sheet.row(r -> {
            r.cell(c -> {
                c.text("Issue details");
                StyleTools.bold(c);
                StyleTools.setAroundThinBlackBorders(c);
            });
            r.cell(c -> {
                c.text("Status");
                StyleTools.bold(c);
                StyleTools.setAroundThinBlackBorders(c);
            });
            r.cell(c -> {
                c.text("Epic Link");
                StyleTools.bold(c);
                StyleTools.setAroundThinBlackBorders(c);
            });
        });
    }

    private static void writeRole(String role, SheetDefinition sheet) {
        sheet.row(r -> r.cell(c -> {
            c.text(role);
            StyleTools.bold(c);
            c.colspan(4);
            StyleTools.setAroundThinBlackBorders(c);
        }));
    }

    private double writeData(String role, List<Person> persons, SheetDefinition sheet) {
        double totalRoleTime = 0.0;
        for (Person person : persons) {
            double totalHours = person.getTotalHours();
            totalRoleTime += totalHours;
            sheet.row(r -> {
                r.cell(c -> {
                    c.text(String.format("%s (%s)", person.getName(), person.getEmail()));
                    StyleTools.bold(c);
                    c.colspan(3);
                    StyleTools.setAroundThinBlackBorders(c);
                });
                r.cell(c -> {
                    c.value(totalHours);
                    StyleTools.bold(c);
                    StyleTools.setAroundThinBlackBorders(c);
                });
            });
            writeTasks(sheet, person);
        }

        writeTotalRoleTime(role, sheet, totalRoleTime);
        return totalRoleTime;
    }

    private void writeTasks(SheetDefinition sheet, Person person) {
        for (Task task : person.getTasks()) {
            String parentTask;
            if (task.getParent().length() != 0) {
                parentTask = task.getParent() + " - ";
            } else {
                parentTask = "";
            }
            double hours = DateTimeTool.getHours(task.getTimeSeconds());
            sheet.row(r -> {
                r.cell(c -> {
                    c.text(parentTask + task.getNumber() + " -");
                    c.text(task.getName());
                    c.width(Auto.AUTO);
                    StyleTools.setAroundThinBlackBorders(c);
                });
                r.cell(c -> {
                    c.text(task.getStatus());
                    c.width(Keywords.Auto.AUTO);
                    StyleTools.setAroundThinBlackBorders(c);
                });
                r.cell(c -> {
                    c.text(task.getEpicLink());
                    c.width(Keywords.Auto.AUTO);
                    StyleTools.setAroundThinBlackBorders(c);
                });
                r.cell(c -> {
                    c.value(hours);
                    StyleTools.setAroundThinBlackBorders(c);
                });
            });
        }
    }

    private static void writeTotalRoleTime(String role, SheetDefinition sheet, double totalRoleTime) {
        sheet.row(r -> {
            r.cell(c -> {
                c.text(role + " Total");
                StyleTools.bold(c);
                c.colspan(3);
                c.style(st -> st.align(center, right));
                StyleTools.setAroundThinBlackBorders(c);
            });
            r.cell(c -> {
                c.value(totalRoleTime);
                StyleTools.bold(c);
                StyleTools.setAroundThinBlackBorders(c);
            });
        });
    }

    private static void writeFooter(SheetDefinition sheet, double grandTotalTime) {
        sheet.row(r -> {
            r.cell(c -> {
                c.text("Grand Total");
                StyleTools.bold(c);
                c.colspan(3);
                c.style(st -> st.align(center, right));
                StyleTools.setAroundThinBlackBorders(c);
            });
            r.cell(c -> {
                c.value(grandTotalTime);
                StyleTools.bold(c);
                StyleTools.setAroundThinBlackBorders(c);
            });
        });
    }
}