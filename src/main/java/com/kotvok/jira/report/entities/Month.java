package com.kotvok.jira.report.entities;

public enum Month {
    Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sept, Oct, Nov, Dec
}
