package com.kotvok.jira.report.entities;

import com.kotvok.jira.report.tools.DateTimeTool;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;

public class Person {

    private String name;
    private String email;
    private List<Task> tasks;
    private BigDecimal price;
    private BigDecimal vatRate;

    public Person() {
    }

    public Person(String name, String email, List<Task> tasks) {
        this.name = name;
        this.email = email;
        this.tasks = tasks;
    }

    public Person(String name, String email, List<Task> tasks, BigDecimal price, BigDecimal vatRate) {
        this.name = name;
        this.email = email;
        this.tasks = tasks;
        this.price = price;
        this.vatRate = vatRate;
    }

    public void sortTasksBySpentTime(){
        getTasks().sort(Comparator.comparing(Task::getTimeSeconds).reversed());
    }

    public double getTotalHours (){
        double totalHours = 0.0;
        for (Task task : getTasks()){
            totalHours += DateTimeTool.getHours(task.getTimeSeconds());
        }
        return totalHours;
    }

    public BigDecimal getAmount() {
        return BigDecimal.valueOf(getTotalHours()).multiply(price);
    }

    public BigDecimal getAmountWithVat() {
        BigDecimal vat = vatRate.divide(BigDecimal.valueOf(100),10, RoundingMode.HALF_UP).add(BigDecimal.valueOf(1));
        BigDecimal amountVat = getAmount().multiply(vat);
        amountVat = amountVat.setScale(2, RoundingMode.HALF_UP);
        return amountVat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getVatRate() {
        return vatRate;
    }

    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }
}
