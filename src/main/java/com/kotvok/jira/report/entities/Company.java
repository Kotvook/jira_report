package com.kotvok.jira.report.entities;

public class Company {

    private String name;
    private String code;
    private String address;
    private String vatId;
    private String bankName;
    private String bankAccount;
    private String bankAddress;
    private String swift;
    private String corrBankAndAddress;
    private String corrBankAndSwift;

    public Company(String name,
                   String code,
                   String address,
                   String vatId,
                   String bankName,
                   String bankAccount,
                   String bankAddress,
                   String swift,
                   String corrBankAndAddress,
                   String corrBankAndSwift
    ) {
        this.name = name;
        this.code = code;
        this.address = address;
        this.vatId = vatId;
        this.bankName = bankName;
        this.bankAccount = bankAccount;
        this.bankAddress = bankAddress;
        this.swift = swift;
        this.corrBankAndAddress = corrBankAndAddress;
        this.corrBankAndSwift = corrBankAndSwift;
    }

    public Company() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }

    public String getCorrBankAndAddress() {
        return corrBankAndAddress;
    }

    public void setCorrBankAndAddress(String corrBankAndAddress) {
        this.corrBankAndAddress = corrBankAndAddress;
    }

    public String getCorrBankAndSwift() {
        return corrBankAndSwift;
    }

    public void setCorrBankAndSwift(String corrBankAndSwift) {
        this.corrBankAndSwift = corrBankAndSwift;
    }
}