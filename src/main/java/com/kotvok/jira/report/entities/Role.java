package com.kotvok.jira.report.entities;

import java.util.List;

public class Role {

    String roleName;
    List<Person> personList;

    public Role(String roleName, List<Person> personList) {
        this.roleName = roleName;
        this.personList = personList;
    }

    public void sortPersonsByEmailList(List<String> emailList){
        personList.sort((person1, person2) -> {
            int index1 = emailList.indexOf(person1.getEmail());
            int index2 = emailList.indexOf(person2.getEmail());
            return Integer.compare(index1, index2);
        });
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }
}
