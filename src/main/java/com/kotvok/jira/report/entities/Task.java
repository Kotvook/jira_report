package com.kotvok.jira.report.entities;

public class Task {

    private final String number;
    private final String parent;
    private final String name;
    private final String status;
    private final String epicLink;
    private int timeSeconds;

    public Task(String number, String parent, String name, String status, String epicLink) {
        this.number = number;
        this.parent = parent;
        this.name = name;
        this.status = status;
        this.epicLink = epicLink;
    }

    public Task(String number, String parent, String name, String status, String epicLink, int timeSeconds) {
        this.number = number;
        this.parent = parent;
        this.name = name;
        this.status = status;
        this.epicLink = epicLink;
        this.timeSeconds = timeSeconds;
    }

    public String getNumber() {
        return number;
    }

    public String getParent() {
        return parent;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getEpicLink() {
        return epicLink;
    }

    public int getTimeSeconds() {
        return timeSeconds;
    }
}