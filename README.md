# Jira Report
___
The application returns an exel file with monthly reports for the specified Jira project
## Setup configure file
To use app change file `src\main\resources\application.properties`:  
 - add your Username, Password and emails for set-up roles for your team (`roles.developer.list` and `roles.qa.list`)
 - add companies data (`companies.supplier` and `companies.customer`)
 - add staff data (`invoice.persons`)
## Usage

1. **Summary table from Jira for all completed tasks for the specified month**:   
http://localhost:8080/api/jira/get-report/{project-key}/{date}/{addParameter}  
`project-key` - name(key) of your project  
`date` - to set up date, write year `yyyy` -> some symbol -> then month `mm` - for ex.(2024-01)  
`addParameter` - the optional parameter but if you specify `all` then the report will show all the people
who participated in the project not just those who are listed in application.yml
if you enter incorrect data in the date, you will receive a report for the current month in response

2. **Invoice according to data from Jira**:  
http://localhost:8080/api/invoice/get-report/{project-key}
`project-key` - name(key) of your project  
The invoice will be drawn up for the previous month
