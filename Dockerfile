FROM amazoncorretto:17.0.3-alpine
USER root
WORKDIR /app/
COPY build/libs/*.jar .
CMD ["java", "-jar", "/app/jira.report-0.0.1-SNAPSHOT.jar"]
RUN apk add --no-cache msttcorefonts-installer fontconfig
RUN update-ms-fonts